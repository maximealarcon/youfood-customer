package com.rman.youfoodcustomer.entity;



public class Waiter {

	private Long id;

	private String lastname;

	private String firstname;

	private Long restaurant;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public Long getRestaurant() {
		return restaurant;
	}
	public void setRestaurant(Long restaurant) {
		this.restaurant = restaurant;
	}
}
