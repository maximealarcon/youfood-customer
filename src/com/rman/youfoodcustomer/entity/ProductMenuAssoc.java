package com.rman.youfoodcustomer.entity;



public class ProductMenuAssoc {

	private Long id;

	private Long menu;

	private Long product;
	public Long getMenu() {
		return menu;
	}
	public void setMenu(Long menu) {
		this.menu = menu;
	}
	public Long getProduct() {
		return product;
	}
	public void setProduct(Long product) {
		this.product = product;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
