package com.rman.youfoodcustomer.entity;

import java.text.NumberFormat;

public class InstructionMenu {
	
	private Long id;
	private Product starter;
	private Product principal;
	private Product desert;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Product getStarter() {
		return starter;
	}
	public void setStarter(Product starter) {
		this.starter = starter;
	}
	public Product getPrincipal() {
		return principal;
	}
	public void setPrincipal(Product principal) {
		this.principal = principal;
	}
	public Product getDesert() {
		return desert;
	}
	public void setDesert(Product desert) {
		this.desert = desert;
	}
	
	public String getDescription(){
		String desc = "";
		NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);
        nf.setGroupingUsed(false); 
		
		if(this.starter != null){
			desc = desc + (desc.equals("") ? "" : " - ") + this.starter.getTitle() + " (" + nf.format(this.starter.getPrice()) + "€)";
		}
		if(this.principal != null){
			desc = desc + (desc.equals("") ? "" : " - ") + this.principal.getTitle() + " (" + nf.format(this.principal.getPrice()) + "€)";
		}
		if(this.desert != null){
			desc = desc + (desc.equals("") ? "" : " - ") + this.desert.getTitle() + " (" + nf.format(this.desert.getPrice()) + "€)";
		}
		return desc.equals("") ? "Vous n'avez sélectionné aucun produit pour ce menu" : desc;
	}
	
	
	public boolean isValid(){
		return (this.starter != null || this.principal != null || this.desert != null) ? true : false;
	}
	
	public Float getPrice(){
		Float price = new Float(0);
		if(this.starter != null) price += starter.getPrice();
		if(this.principal != null) price += principal.getPrice();
		if(this.desert != null) price += desert.getPrice();
		return price;
	}
}
