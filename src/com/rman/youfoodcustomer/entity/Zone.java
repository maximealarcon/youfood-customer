package com.rman.youfoodcustomer.entity;

import java.util.List;


public class Zone {

	private Long id;
	private List<Tabula> tables;
	private String name;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Tabula> getTables() {
		return tables;
	}
	public void setTables(List<Tabula> tables) {
		this.tables = tables;
	}
}
