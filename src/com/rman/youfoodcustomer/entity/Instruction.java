package com.rman.youfoodcustomer.entity;

import java.util.Date;
import java.util.List;


public class Instruction {
	private Long id;
	private Date creationDate;
	private List<InstructionMenu> menus;
	private Long table;
	private boolean served;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public List<InstructionMenu> getMenus() {
		return menus;
	}
	public void setMenus(List<InstructionMenu> menus) {
		this.menus = menus;
	}
	public Long getTable() {
		return table;
	}
	public void setTable(Long table) {
		this.table = table;
	}
	public boolean getServed() {
		return served;
	}
	public void setServed(boolean served) {
		this.served = served;
	}
	
	
	public boolean isValid(){
		boolean valid = true;
		if(this.menus == null || this.menus.size() < 1){
			valid = false;
		} else {
			for(int i = 0; i < this.menus.size(); i++){
				if (!this.menus.get(i).isValid()) valid = false;
			}
		}
		return valid;
	}
	
	public Float getPrice(){
		Float price = new Float(0);
		if(this.menus != null && this.menus.size() > 0){
			for(int i = 0; i < this.menus.size(); i++){
				price += this.menus.get(i).getPrice();
			}
		}
		return price;
	}
}
