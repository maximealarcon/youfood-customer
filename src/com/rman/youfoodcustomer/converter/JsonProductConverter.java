package com.rman.youfoodcustomer.converter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rman.youfoodcustomer.entity.Product;

public class JsonProductConverter {

	public static final String ID_FIELD = "id";
	public static final String TITLE_FIELD = "title";
	public static final String TYPE_FIELD = "type";
	public static final String PRICE_FIELD = "price";
	
	
	public static Product JsonToProduct(JSONObject json) throws JSONException {
		Product product = new Product();
		product.setId(json.getLong(ID_FIELD));
		product.setTitle(json.getString(TITLE_FIELD));
		product.setType(json.getInt(TYPE_FIELD));
		product.setPrice(Float.valueOf(json.getString(PRICE_FIELD)));
		
		return product;
	}
	
	public static JSONObject ProductToJson(Product product) throws JSONException {
		JSONObject json = new JSONObject();
		json.put(ID_FIELD, product.getId());
		json.put(TITLE_FIELD, product.getTitle());
		json.put(TYPE_FIELD, product.getType());
		json.put(PRICE_FIELD, product.getPrice());
		
		return json;
	}
	
	public static List<Product> JsonToProductList(JSONObject json) throws JSONException {
		ArrayList<Product> productList = new ArrayList<Product>();
		if(json.has("product")){
			JSONArray productElements = json.optJSONArray("product");
			if(productElements == null){
				//Un résultat
				productList.add(JsonProductConverter.JsonToProduct(json.optJSONObject("product")));
			} else {
				//Plusieurs résultats
				for (int i = 0; i < productElements.length(); i++){
					productList.add(JsonProductConverter.JsonToProduct(productElements.getJSONObject(i)));
				}
			}
		}
		return productList;
	}
	
}
