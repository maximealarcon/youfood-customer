package com.rman.youfoodcustomer.converter;

import org.json.JSONException;
import org.json.JSONObject;

import com.rman.youfoodcustomer.entity.Menu;

public class JsonMenuConverter {
	
	public static final String ID_FIELD = "id";
	public static final String TITLE_FIELD = "title";
	
	
	public static Menu JsonToMenu(JSONObject json) throws JSONException {
		Menu menu = new Menu();
		menu.setId(json.getLong(ID_FIELD));
		menu.setTitle(json.getString(TITLE_FIELD));
		
		return menu;
	}
	
	public static JSONObject MenuToJson(Menu menu) throws JSONException {
		JSONObject json = new JSONObject();
		json.put(ID_FIELD, menu.getId());
		json.put(TITLE_FIELD, menu.getTitle());
		
		return json;
	}
}
