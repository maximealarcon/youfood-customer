package com.rman.youfoodcustomer.converter;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rman.youfoodcustomer.entity.InstructionMenu;

public class JsonInstructionMenuConverter {

	public static final String ID_FIELD = "id";
	public static final String STARTER_FIELD = "starter";
	public static final String PRINCIPAL_FIELD = "principal";
	public static final String DESERT_FIELD = "desert";
	
	
	public static JSONObject InstructionMenuToJson(InstructionMenu menu) throws JSONException {
		JSONObject json = new JSONObject();
		json.put(ID_FIELD, menu.getId());
		json.put(STARTER_FIELD, menu.getStarter() != null ? JsonProductConverter.ProductToJson(menu.getStarter()) : "null");
		json.put(PRINCIPAL_FIELD, menu.getPrincipal() != null ? JsonProductConverter.ProductToJson(menu.getPrincipal()) : "null");
		json.put(DESERT_FIELD, menu.getDesert() != null ? JsonProductConverter.ProductToJson(menu.getDesert()) : "null");
		
		return json;
	}
	
	public static JSONArray InstructionMenuListToJson(List<InstructionMenu> menus) throws JSONException {
		JSONArray json = new JSONArray();
		for (int i = 0; i < menus.size(); i++){
			json.put(JsonInstructionMenuConverter.InstructionMenuToJson(menus.get(i)));
		}
		return json;
	}
}
