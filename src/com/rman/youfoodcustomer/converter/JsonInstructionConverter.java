package com.rman.youfoodcustomer.converter;

import org.json.JSONException;
import org.json.JSONObject;

import com.rman.youfoodcustomer.entity.Instruction;

public class JsonInstructionConverter {

	public static final String ID_FIELD = "id";
	public static final String DATE_FIELD = "creationDate";
	public static final String MENUS_FIELD = "menus";
	public static final String TABLE_FIELD = "table";
	public static final String SERVED_FIELD = "served";
	
	
	public static JSONObject InstructionToJson(Instruction instruction) throws JSONException {
		JSONObject json = new JSONObject();
		json.put(ID_FIELD, instruction.getId());
		json.put(DATE_FIELD, instruction.getCreationDate());
		json.put(MENUS_FIELD, JsonInstructionMenuConverter.InstructionMenuListToJson(instruction.getMenus()));
		json.put(TABLE_FIELD, instruction.getTable());
		json.put(SERVED_FIELD, instruction.getServed());
		return json;
	}
}
