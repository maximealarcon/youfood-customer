package com.rman.youfoodcustomer.converter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rman.youfoodcustomer.entity.Zone;

public class JsonZoneConverter {

	public static final String ID_FIELD = "id";
	public static final String NAME_FIELD = "name";
	public static final String TABLES_FIELD = "tables";
	
	public static List<Zone> JsonToZoneList(JSONObject json) throws JSONException {
		ArrayList<Zone> zoneList = new ArrayList<Zone>();
		if(json.has("zones")){
			JSONArray zoneElements = json.optJSONArray("zones");
			if(zoneElements == null){
				//Un résultat
				zoneList.add(JsonZoneConverter.JsonToZone(json.optJSONObject("zones")));
			} else {
				//Plusieurs résultats
				for (int i = 0; i < zoneElements.length(); i++){
					zoneList.add(JsonZoneConverter.JsonToZone(zoneElements.getJSONObject(i)));
				}
			}
		}
		return zoneList;
	}
	
	public static Zone JsonToZone(JSONObject json) throws JSONException {
		Zone zone = new Zone();
		zone.setId(json.getLong(ID_FIELD));
		zone.setName(json.getString(NAME_FIELD));
		zone.setTables(JsonTabulaConverter.JsonToTabulaList(json));
		
		return zone;
	}
}
