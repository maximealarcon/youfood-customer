package com.rman.youfoodcustomer.converter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rman.youfoodcustomer.entity.Tabula;

public class JsonTabulaConverter {

	public static final String ID_FIELD = "id";
	public static final String NAME_FIELD = "name";
	
	public static List<Tabula> JsonToTabulaList(JSONObject json) throws JSONException {
		ArrayList<Tabula> tableList = new ArrayList<Tabula>();
		if(json.has("tables")){
			JSONArray tableElements = json.optJSONArray("tables");
			if(tableElements == null){
				//Un résultat
				tableList.add(JsonTabulaConverter.JsonToTabula(json.optJSONObject("tables")));
			} else {
				//Plusieurs résultats
				for (int i = 0; i < tableElements.length(); i++){
					tableList.add(JsonTabulaConverter.JsonToTabula(tableElements.getJSONObject(i)));
				}
			}
		}
		return tableList;
	}
	
	public static Tabula JsonToTabula(JSONObject json) throws JSONException {
		Tabula tabula = new Tabula();
		tabula.setId(json.getLong(ID_FIELD));
		tabula.setName(json.getString(NAME_FIELD));
		
		return tabula;
	}
}
