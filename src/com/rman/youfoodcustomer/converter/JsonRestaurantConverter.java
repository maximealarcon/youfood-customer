package com.rman.youfoodcustomer.converter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rman.youfoodcustomer.entity.Restaurant;

public class JsonRestaurantConverter {

	public static final String ID_FIELD = "id";
	public static final String NAME_FIELD = "name";
	public static final String ZONES_FIELD = "zones";
	
	public static List<Restaurant> JsonToRestaurantList(JSONObject json) throws JSONException {
		ArrayList<Restaurant> restaurantList = new ArrayList<Restaurant>();
		if(json.has("restaurant")){
			JSONArray restaurantElements = json.optJSONArray("restaurant");
			if(restaurantElements == null){
				//Un résultat
				restaurantList.add(JsonRestaurantConverter.JsonToRestaurant(json.optJSONObject("restaurant")));
			} else {
				//Plusieurs résultats
				for (int i = 0; i < restaurantElements.length(); i++){
					restaurantList.add(JsonRestaurantConverter.JsonToRestaurant(restaurantElements.getJSONObject(i)));
				}
			}
		}
		return restaurantList;
	}
	
	public static Restaurant JsonToRestaurant(JSONObject json) throws JSONException {
		Restaurant restaurant = new Restaurant();
		restaurant.setId(json.getLong(ID_FIELD));
		restaurant.setName(json.getString(NAME_FIELD));
		restaurant.setZones(JsonZoneConverter.JsonToZoneList(json));
		
		return restaurant;
	}
}
