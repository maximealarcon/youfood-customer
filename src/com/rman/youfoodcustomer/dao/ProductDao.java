package com.rman.youfoodcustomer.dao;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.rman.youfoodcustomer.converter.JsonProductConverter;
import com.rman.youfoodcustomer.entity.Product;
import com.rman.youfoodcustomer.util.HttpHelper;
import com.rman.youfoodcustomer.util.ResourceNotFoundException;

public class ProductDao {

private static String API_PRODUCT_RELATIVE_URI = "/product";
	
	public static List<Product> getProductsForMenu(Long idMenu) throws ResourceNotFoundException, JSONException {
		String response = HttpHelper.get(HttpHelper.API_URI + API_PRODUCT_RELATIVE_URI + "/"+idMenu);
		return response.equals("null") ? null : JsonProductConverter.JsonToProductList(new JSONObject(response));
	}
}
