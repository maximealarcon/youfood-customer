package com.rman.youfoodcustomer.dao;

import org.json.JSONException;
import org.json.JSONObject;

import com.rman.youfoodcustomer.converter.JsonMenuConverter;
import com.rman.youfoodcustomer.entity.Menu;
import com.rman.youfoodcustomer.util.HttpHelper;
import com.rman.youfoodcustomer.util.ResourceNotFoundException;

public class MenuDao {
	
	private static String API_MENU_RELATIVE_URI = "/menu";
	
	public static Menu getCurrentMenuForRestaurant(Long idRestaurant) throws ResourceNotFoundException, JSONException {
		String response = HttpHelper.get(HttpHelper.API_URI + API_MENU_RELATIVE_URI + "/current/"+idRestaurant);
		return JsonMenuConverter.JsonToMenu(new JSONObject(response));
	}
}
