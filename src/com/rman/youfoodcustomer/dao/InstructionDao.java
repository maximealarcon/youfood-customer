package com.rman.youfoodcustomer.dao;

import org.json.JSONException;

import com.rman.youfoodcustomer.converter.JsonInstructionConverter;
import com.rman.youfoodcustomer.entity.Instruction;
import com.rman.youfoodcustomer.util.HttpHelper;
import com.rman.youfoodcustomer.util.ResourceNotFoundException;

public class InstructionDao {

	private static String API_INSTRUCTION_RELATIVE_URI = "/instruction";
	
	public static void addInstruction(Instruction instruction) throws ResourceNotFoundException, JSONException {
		HttpHelper.post((HttpHelper.API_URI + API_INSTRUCTION_RELATIVE_URI), JsonInstructionConverter.InstructionToJson(instruction));
	}
}
