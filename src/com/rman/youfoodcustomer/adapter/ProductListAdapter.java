package com.rman.youfoodcustomer.adapter;

import java.text.NumberFormat;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rman.youfoodcustomer.activity.R;
import com.rman.youfoodcustomer.entity.Product;

public class ProductListAdapter extends BaseAdapter {

	private List<Product> objects;
	private LayoutInflater inflater;
	
	public ProductListAdapter(Context context, List<Product> objects) {
		this.objects = objects;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		return objects.size();
	}

	@Override
	public Product getItem(int position) {
		return objects.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return this.createViewFromResource(position, convertView, parent);
	}
	
	private View createViewFromResource(int position, View convertView, ViewGroup parent){
		View view;
        TextView name;
        TextView price;
        
        if (convertView == null) {
            view = inflater.inflate(R.layout.productslistrow, parent, false);
        } else {
            view = convertView;
        }
        
        name = (TextView) view.findViewById(R.id.productslistrowTitle);
        price = (TextView) view.findViewById(R.id.productslistrowSubtitle);
        name.setText(objects.get(position).getTitle());
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);
        nf.setGroupingUsed(false); 
        price.setText(nf.format(objects.get(position).getPrice()) + "�");
        
        return view;
	}
}
