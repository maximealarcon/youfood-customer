package com.rman.youfoodcustomer.adapter;

import java.text.NumberFormat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rman.youfoodcustomer.activity.R;
import com.rman.youfoodcustomer.entity.InstructionMenu;
import com.rman.youfoodcustomer.entity.Product;

public class InstructionMenuAdapter extends BaseAdapter {
	
	private InstructionMenu menu;
	private LayoutInflater inflater;
	
	public InstructionMenuAdapter(Context context, InstructionMenu menu) {
		this.menu = menu;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return 3;
	}

	@Override
	public Product getItem(int position) {
		Product result = null;
		switch(position){
			case 0:
				result = this.menu.getStarter();
				break;
			case 1:
				result = this.menu.getPrincipal();
				break;
			case 2:
				result = this.menu.getDesert();
				break;
		}
		return result;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return this.createViewFromResource(position, convertView, parent);
	}
	
	private View createViewFromResource(int position, View convertView, ViewGroup parent){
		View view;
        TextView name;
        TextView price;
        
        if (convertView == null){
            view = inflater.inflate(R.layout.menurow, parent, false);
        } else {
            view = convertView;
        }
        
        name = (TextView) view.findViewById(R.id.menurowTitle);
        price = (TextView) view.findViewById(R.id.menurowSubtitle);
        
        Product p = (Product) getItem(position);
        String label = "";
        switch(position){
			case 0:
				label = "Entr�e : ";
				break;
			case 1:
				label = "Plat : ";
				break;
			case 2:
				label = "Dessert : ";
				break;
		}
        label += p != null ? p.getTitle() : " - ";
        name.setText(label);
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);
        nf.setGroupingUsed(false); 
        price.setText((p != null ? nf.format(p.getPrice()) : "0,00") + "�");
        
        return view;
	}
}
