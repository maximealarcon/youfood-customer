package com.rman.youfoodcustomer.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rman.youfoodcustomer.activity.R;
import com.rman.youfoodcustomer.entity.InstructionMenu;

public class InstructionMenuListAdapter extends BaseAdapter {

	private List<InstructionMenu> objects;
	private LayoutInflater inflater;
	
	public InstructionMenuListAdapter(Context context, List<InstructionMenu> objects) {
		this.objects = objects;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		return objects.size();
	}

	@Override
	public InstructionMenu getItem(int position) {
		return objects.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return this.createViewFromResource(position, convertView, parent);
	}
	
	private View createViewFromResource(int position, View convertView, ViewGroup parent){
		View view;
        
        if (convertView == null) {
            view = inflater.inflate(R.layout.menuslistrow, parent, false);
        } else {
            view = convertView;
        }
        
        TextView title = (TextView) view.findViewById(R.id.menuslistrowTitle);
        TextView subtitle = (TextView) view.findViewById(R.id.menuslistrowSubtitle);
        
        InstructionMenu i = (InstructionMenu) getItem(position);
        title.setText("Menu " + (position+1));
        subtitle.setText(i.getDescription());
        
        return view;
	}
}
