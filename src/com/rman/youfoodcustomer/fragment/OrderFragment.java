package com.rman.youfoodcustomer.fragment;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.rman.youfoodcustomer.activity.NewOrderActivity;
import com.rman.youfoodcustomer.activity.R;
import com.rman.youfoodcustomer.adapter.InstructionMenuListAdapter;
import com.rman.youfoodcustomer.entity.Instruction;
import com.rman.youfoodcustomer.entity.InstructionMenu;

public class OrderFragment extends Fragment {
	
	//List
	public List<InstructionMenu> menus;
	private ListView menusListView;
	
	//Text
	private TextView orderRecap;
	
	//Buttons
	private Button newMenuButton;
	
	//Info
	private int selectedMenu;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.orderfragment, container, false);
		
		//Récupération des élements
		this.menusListView = (ListView) view.findViewById(R.id.menusListView);
		this.newMenuButton = (Button) view.findViewById(R.id.newMenuButton);
		this.orderRecap = (TextView) view.findViewById(R.id.orderRecap);
		
		//Initialisation liste des menus
		this.menus = new ArrayList<InstructionMenu>();
		
		this.updateOrderRecap();
		
		
		//Clique sur nouveau menu
		this.newMenuButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				menus.add(new InstructionMenu());
				notifyMenusAdapter();
				updateOrderRecap();
			}
		});
		
		
		//Clique sur la liste
		this.menusListView.setOnItemClickListener(new OnItemClickListener() {
			@SuppressWarnings("rawtypes")
			@Override
			public void onItemClick(AdapterView a, View v, int position, long id) {
				selectedMenu = position;
				((NewOrderActivity) getActivity()).menuSelected(position, menus.get(position));
			}
		});
		
		return view;
	}
	
	public Instruction getInstructionForSending(){
		Instruction i = new Instruction();
    	i.setCreationDate(new Date());
    	i.setMenus(menus);
    	i.setTable(((NewOrderActivity) getActivity()).table);
    	i.setServed(false);
    	return i;
	}
	
	private void notifyMenusAdapter(){
		if(this.menus.size() > 0){
			if(this.menusListView.getAdapter() != null){
				((InstructionMenuListAdapter) this.menusListView.getAdapter()).notifyDataSetChanged();
			} else {
				this.menusListView.setAdapter(new InstructionMenuListAdapter(getActivity() ,this.menus));
			}
		} else {
			this.menusListView.setAdapter(null);
		}
	}
	
	private void updateOrderRecap(){
		String recap = "";
		if(this.menus.size() > 0){
			NumberFormat nf = NumberFormat.getInstance();
	        nf.setMinimumFractionDigits(2);
	        nf.setMaximumFractionDigits(2);
	        nf.setGroupingUsed(false); 
			Instruction i = new Instruction();
			i.setMenus(this.menus);
			recap = this.menus.size() + " menu(s) - Prix total : " + nf.format(i.getPrice()) + "€";
		} else {
			recap = "Aucun menu pour le moment";
		}
		this.orderRecap.setText(recap);
	}
	
	public void updateCurrentMenu(InstructionMenu menu){
		this.menus.set(this.selectedMenu, menu);
		this.notifyMenusAdapter();
		this.updateOrderRecap();
	}
	
	public void deleteCurrentMenu(){
		this.menus.remove(this.selectedMenu);
		this.selectedMenu = 0;
		this.notifyMenusAdapter();
		this.updateOrderRecap();
	}
}
