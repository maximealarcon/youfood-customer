package com.rman.youfoodcustomer.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.rman.youfoodcustomer.activity.NewOrderActivity;
import com.rman.youfoodcustomer.activity.R;
import com.rman.youfoodcustomer.adapter.InstructionMenuAdapter;
import com.rman.youfoodcustomer.entity.InstructionMenu;
import com.rman.youfoodcustomer.entity.Product;

public class MenuFragment extends Fragment {
	
	//List
	private InstructionMenu menu;
	private ListView menuListView;
	
	//Buttons
	private Button deleteMenuButton;
	
	//Title
	private TextView title;
	
	//Info
	private int selectedProductType;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.menufragment, container, false);
		
		//Récupération des élements
		this.menuListView = (ListView) view.findViewById(R.id.menuListView);
		this.title = (TextView) view.findViewById(R.id.menuFragmentTitle);
		this.deleteMenuButton = (Button) view.findViewById(R.id.deleteMenuButton);
		
		//Clique sur supprimer menu
		this.deleteMenuButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//Confimation
				new AlertDialog.Builder(getActivity())
		        .setIcon(R.drawable.ic_error)
		        .setTitle("Attention")
		        .setMessage("Voulez-vous vraiment supprimer ce menu ?")
		        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface dialog, int which) {
		            	((NewOrderActivity) getActivity()).menuDeleted();
		            }

		        })
		        .setNegativeButton("Non", null)
		        .show();
			}
		});
		
		//Clique sur la liste
		this.menuListView.setOnItemClickListener(new OnItemClickListener() {
			@SuppressWarnings("rawtypes")
			@Override
			public void onItemClick(AdapterView a, View v, int position, long id) {
				selectedProductType = position;
				((NewOrderActivity) getActivity()).productTypeSelected(position);
			}
		});
		
		
		return view;
	}
	
	private void notifyMenuAdapter(){
		this.menuListView.setAdapter(new InstructionMenuAdapter(getActivity() ,this.menu));
	}
	
	public void setMenu(int position, InstructionMenu menu){
		this.menu = menu;
		this.title.setText("Menu " + (position+1));
		this.notifyMenuAdapter();
	}
	
	public void setProduct(Product p){
		switch(this.selectedProductType){
			case 0:
				this.menu.setStarter(p);
				break;
			case 1:
				this.menu.setPrincipal(p);
				break;
			case 2:
				this.menu.setDesert(p);
				break;
		}
		this.notifyMenuAdapter();
		((NewOrderActivity) getActivity()).updateMenu(this.menu);
	}
}
