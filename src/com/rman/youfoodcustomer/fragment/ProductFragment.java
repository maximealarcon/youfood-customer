package com.rman.youfoodcustomer.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.rman.youfoodcustomer.activity.NewOrderActivity;
import com.rman.youfoodcustomer.activity.R;
import com.rman.youfoodcustomer.adapter.ProductListAdapter;
import com.rman.youfoodcustomer.entity.Product;

public class ProductFragment extends Fragment {
	
	//Lists
	private List<Product> starters;
	private List<Product> principals;
	private List<Product> deserts;
	private ListView productsListView;
	
	//Buttons
	private Button deleteProductButton;
	
	//Title
	private TextView title;
	
	//Info
	private int productType;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.productfragment, container, false);
		
		//R�cup�ration des �lements
		this.productsListView = (ListView) view.findViewById(R.id.productsListView);
		this.title = (TextView) view.findViewById(R.id.productFragmentTitle);
		this.deleteProductButton = (Button) view.findViewById(R.id.deleteProductButton);
		
		//R�cup�ration des produits
		this.starters = new ArrayList<Product>();
		this.principals = new ArrayList<Product>();
		this.deserts = new ArrayList<Product>();
		//this.getProducts();
		
		//Clique sur supprimer produit
		this.deleteProductButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((NewOrderActivity) getActivity()).productSelected(null);
			}
		});
		
		//Clique sur la liste
		this.productsListView.setOnItemClickListener(new OnItemClickListener() {
			@SuppressWarnings("rawtypes")
			@Override
			public void onItemClick(AdapterView a, View v, int position, long id) {
				((NewOrderActivity) getActivity()).productSelected(getSelectedProduct(position));
			}
		});
		
		return view;
	}
	
	public void setProducts(List<Product> products){
		if(products != null){
			for (int i = 0; i < products.size(); i++){
				switch(products.get(i).getType()){
					case 0:
						this.starters.add(products.get(i));
						break;
					case 1:
						this.principals.add(products.get(i));
						break;
					case 2:
						this.deserts.add(products.get(i));
						break;
				}
			}
		}
	}
	
	private void notifyProductAdapter(){
		List<Product> p = null;
		switch(this.productType){
			case 0: 
				p = this.starters;
				break;
			case 1:
				p = this.principals;
				break;
			case 2:
				p = this.deserts;
				break;
		}
		
		if(p.size() > 0){
			this.productsListView.setAdapter(new ProductListAdapter(getActivity() ,p));
		} else {
			this.productsListView.setAdapter(null);
		}
	}
	
	private Product getSelectedProduct(int position){
		Product p = null;
		switch(productType){
			case 0:
				p = this.starters.get(position);
				break;
			case 1:
				p = this.principals.get(position);
				break;
			case 2:
				p = this.deserts.get(position);
				break;
		}
		return p;
	}
	
	public void setProductType(int productType){
		this.productType = productType;
		this.notifyProductAdapter();
		switch(productType){
			case 0:
				this.title.setText("Choisir une entr�e");
				break;
			case 1:
				this.title.setText("Choisir un plat");
				break;
			case 2:
				this.title.setText("Choisir un dessert");
				break;
		}
	}
}
