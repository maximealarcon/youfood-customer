package com.rman.youfoodcustomer.activity;

import java.math.BigDecimal;
import java.util.List;

import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.paypal.android.MEP.CheckoutButton;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalInvoiceData;
import com.paypal.android.MEP.PayPalInvoiceItem;
import com.paypal.android.MEP.PayPalPayment;
import com.rman.youfoodcustomer.dao.InstructionDao;
import com.rman.youfoodcustomer.dao.MenuDao;
import com.rman.youfoodcustomer.dao.ProductDao;
import com.rman.youfoodcustomer.entity.Instruction;
import com.rman.youfoodcustomer.entity.InstructionMenu;
import com.rman.youfoodcustomer.entity.Menu;
import com.rman.youfoodcustomer.entity.Product;
import com.rman.youfoodcustomer.fragment.MenuFragment;
import com.rman.youfoodcustomer.fragment.OrderFragment;
import com.rman.youfoodcustomer.fragment.ProductFragment;
import com.rman.youfoodcustomer.util.Messages;
import com.rman.youfoodcustomer.util.ResourceNotFoundException;

public class NewOrderActivity extends Activity {
	
	//Fragment Manager
	private FragmentManager fm;
	
	//Fragments
	private OrderFragment orderFragment;
	private MenuFragment menuFragment;
	private ProductFragment productFragment;
	
	//Buttons
	private Button cancelOrderButton;
	private Button validateOrderButton;
	
	//Text
	private TextView title;
	
	//Infos
	private Long restaurant;
	public Long table;
	private Menu currentMenu;
	
	//Dialog
	public ProgressDialog pd;
	
	//Payment
	public static int request = 1;
	public AlertDialog paymentPopup;
	public CheckoutButton ppButton;
	public Instruction finalInstruction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.neworder);
		
		//Creation du ProgressDialog
		this.pd = new ProgressDialog(this);
		this.pd.setCancelable(false);
		
		//R�cup�rations des fragments
		this.fm = getFragmentManager();
		this.orderFragment = (OrderFragment)fm.findFragmentById(R.id.orderfragment);
		this.menuFragment = (MenuFragment)fm.findFragmentById(R.id.menufragment);
		this.productFragment = (ProductFragment)fm.findFragmentById(R.id.productfragment);
		
		this.cancelOrderButton = (Button) findViewById(R.id.cancelOrderButton);
		this.validateOrderButton = (Button) findViewById(R.id.validateOrderButton);
		this.title = (TextView) findViewById(R.id.neworderTitle);
		
		this.loadPreferences();
		
		//On cache les deux fragments de droite
		FragmentTransaction ft = this.fm.beginTransaction();
		ft.hide(this.fm.findFragmentById(R.id.menufragment));
		ft.hide(this.fm.findFragmentById(R.id.productfragment));
		ft.commit();
		
		//Clique sur annuler commande
		this.cancelOrderButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new AlertDialog.Builder(NewOrderActivity.this)
		        .setIcon(R.drawable.ic_info)
		        .setTitle("Attention")
		        .setMessage("Voulez-vous vraiment annuler votre commande ?")
		        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface dialog, int which) {
		            	finish();
		            }

		        })
		        .setNegativeButton("Non", null)
		        .show();
			}
		});
		
		//Clique sur valider commande
		this.validateOrderButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Instruction instruction = new Instruction();
				instruction.setMenus(orderFragment.menus);
				if(orderFragment.menus == null || orderFragment.menus.size() < 1){
					Messages.createAlert(NewOrderActivity.this, "Erreur", Messages.ERROR, "Impossible de valider la commande.\nAucun menu n'a �t� cr��.");
				} else if(!instruction.isValid()){
					Messages.createAlert(NewOrderActivity.this, "Erreur", Messages.ERROR, "Impossible de valider la commande.\nUn des menus n'a aucun produit s�lectionn�.");
				} else {
					new AlertDialog.Builder(NewOrderActivity.this)
			        .setIcon(R.drawable.ic_info)
			        .setTitle("Valider la commande")
			        .setMessage("Valider votre commande et proc�der au paiement ?")
			        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
			            @Override
			            public void onClick(DialogInterface dialog, int which) {
			            	initializePayment();
			            }
			        })
			        .setNegativeButton("Non", null)
			        .show();
				}
			}
		});
		
		
		//On r�cup�re les informations n�cessaires sur le serveur
		this.getCurrentMenu(new Long(this.restaurant));
	}
	
	
	private void loadPreferences(){
    	SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        this.table = Long.valueOf(sp.getString("table", "0"));
        this.restaurant = Long.valueOf(sp.getString("restaurant", "0"));
    }
	
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.layout.neworder_menu, menu);
    	
    	return true;
	}
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {		
		case R.id.helpoption:
			Messages.createAlert(this, "Aide", Messages.HELP, "Appuyez sur 'Ajouter un menu' pour commencer.\n\n" +
					"S�lectionnez ensuite votre menu : vous pourrez choisir pour ce menu une entr�e, un plat, et un dessert.\n\n" +
					"Vous pouvez ajouter autant de menus que vous le souhaitez.");
			return true;
		}
    	return false;
    }
	
	
	//Tasks
	//Get current menu for restaurant
	private void getCurrentMenu(final Long idRestaurant){
		new AsyncTask<Void, Void, Menu>(){

			private String errorMessage;
			
			@Override
			protected Menu doInBackground(Void... params) {
				Menu menu = null;
	    		try {
	    			menu = MenuDao.getCurrentMenuForRestaurant(idRestaurant);
	    		} catch (ResourceNotFoundException e) {
	    			this.errorMessage = e.getMessage();
	    			this.cancel(true);
	    		} catch (JSONException j) {
	    			this.errorMessage = "Erreur pendant la r�cup�ration des donn�es";
	    			this.cancel(true);
	    		}
	    		return menu;
			}
			
			protected void onPreExecute() {
				pd.setMessage("Chargement des informations...\nVeuillez patienter...");
				pd.show();
			}
			
			protected void onPostExecute(Menu result){
				currentMenu = result;
				title.setText("Menu actuel : "+currentMenu.getTitle());
				getProducts(currentMenu.getId());
			}
			
			protected void onCancelled() {
				pd.hide();
				Messages.createAlert(NewOrderActivity.this, "Erreur", Messages.ERROR, errorMessage);
			}
			
		}.execute();
	}
	
	
	//Get products for the current menu
	private void getProducts(final Long idMenu){
		new AsyncTask<Void, Void, List<Product>>(){

			private String errorMessage;
			
			@Override
			protected List<Product> doInBackground(Void... params) {
				List<Product> products = null;
	    		try {
	    			products = ProductDao.getProductsForMenu(idMenu);
	    		} catch (ResourceNotFoundException e) {
	    			this.errorMessage = e.getMessage();
	    			this.cancel(true);
	    		} catch (JSONException j) {
	    			this.errorMessage = j.getMessage();
	    			this.cancel(true);
	    		}
	    		return products;
			}
			
			protected void onPostExecute(List<Product> result){
				productFragment.setProducts(result);
				pd.hide();
			}
			
			protected void onCancelled() {
				pd.hide();
				Messages.createAlert(NewOrderActivity.this, "Erreur", Messages.ERROR, errorMessage);
			}
			
		}.execute();
	}
	
	
	//Initialize PayPal
	public void initializePayment(){
		new AsyncTask<Void, Void, Void>(){
			
			@Override
			protected Void doInBackground(Void... params) {
				if(PayPal.getInstance() == null || !PayPal.getInstance().isLibraryInitialized()){
					PayPal.initWithAppID(getBaseContext(), "APP-80W284485P519543T", PayPal.ENV_SANDBOX);
					if(PayPal.getInstance().isLibraryInitialized()){
						PayPal.getInstance().setLanguage("fr_FR");
					} else {
						this.cancel(true);
					}
				}
				return null;
			}
			
			protected void onPreExecute() {
				pd.setMessage("Chargement du module PayPal...\nVeuillez patienter...");
				pd.show();
			}
			
			protected void onPostExecute(Void result) {
				pd.hide();
				NewOrderActivity.this.createPaymentPopup(true);
			}
			
			protected void onCancelled() {
				pd.hide();
				AlertDialog.Builder builder = new AlertDialog.Builder(NewOrderActivity.this);
		        builder.setTitle("PayPal")
		        	   .setIcon(R.drawable.ic_error)
		        	   .setMessage("Impossible de charger le module PayPal.")
		               .setCancelable(false)
		               .setNegativeButton("Fermer", new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog, int id) {
		                	   NewOrderActivity.this.createPaymentPopup(false);
		                   }
		               });
		        AlertDialog alert = builder.create();
		        alert.show();
			}
			
		}.execute();
	}
	
	
	//Send instruction to server
	public void sendInstruction(final Instruction instruction){
		new AsyncTask<Void, Void, Void>(){

			private String errorMessage;
			
			@Override
			protected Void doInBackground(Void... params) {
	    		try {
	    			InstructionDao.addInstruction(instruction);
	    		} catch (ResourceNotFoundException e) {
	    			this.errorMessage = e.getMessage();
	    			this.cancel(true);
	    		} catch (JSONException j) {
	    			this.errorMessage = "Une erreur est survenue pendant le traitement de votre commande.\nVeuillez appeler un employ� pour r�soudre le probl�me";
	    			this.cancel(true);
	    		}
	    		return null;
			}
			
			protected void onPreExecute() {
				pd.setMessage("Traitement de votre commande...\nVeuillez patienter...");
				pd.show();
			}
			
			protected void onPostExecute(Void result){
				pd.hide();
				AlertDialog.Builder builder = new AlertDialog.Builder(NewOrderActivity.this);
		        builder.setTitle("Traitement de votre commande")
		        	   .setIcon(R.drawable.ic_idea)
		        	   .setMessage("Votre commande a bien �t� trait�e.\nYouFood vous remercie de votre visite et vous souhaite un bon app�tit !")
		               .setCancelable(false)
		               .setNegativeButton("Fermer", new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog, int id) {
		                        NewOrderActivity.this.finish();
		                   }
		               });
		        AlertDialog alert = builder.create();
		        alert.show();
			}
			
			protected void onCancelled() {
				pd.hide();
				Messages.createAlert(NewOrderActivity.this, "Erreur", Messages.ERROR, errorMessage);
			}
			
		}.execute();
	}
	
	
	//Generate PayPal Payment from instruction
	public PayPalPayment getPayPalPayment(Instruction instruction){
		PayPalPayment payment = new PayPalPayment();
		payment.setSubtotal(new BigDecimal(Float.toString(instruction.getPrice())));
		payment.setCurrencyType("EUR");
		payment.setRecipient("yfood_1338567483_biz@hotmail.fr");
		payment.setPaymentType(PayPal.PAYMENT_TYPE_GOODS);
		
	    PayPalInvoiceData invoice = new PayPalInvoiceData();
	    invoice.setTax(new BigDecimal("0"));
	    invoice.setShipping(new BigDecimal("0"));

	    for (int i = 0; i < instruction.getMenus().size(); i++){
	    	PayPalInvoiceItem item = new PayPalInvoiceItem();
	    	item.setName("Menu "+(i+1));
	    	item.setTotalPrice(new BigDecimal(Float.toString(instruction.getMenus().get(i).getPrice())));
	    	invoice.add(item);
	    }

	    payment.setInvoiceData(invoice);
	    payment.setMerchantName("YouFood Inc");
		
		return payment;
	}
	
	
	//Create popup for payment methods
	public void createPaymentPopup(boolean paypal){
		finalInstruction = orderFragment.getInstructionForSending();
		
		//Cr�ation de la popup pour les m�thodes de paiement
		LayoutInflater factory = LayoutInflater.from(NewOrderActivity.this);
        View paymentView = factory.inflate(R.layout.paymentdialog, null);
        
        if(paypal){
        	ppButton = PayPal.getInstance().getCheckoutButton(NewOrderActivity.this, PayPal.BUTTON_194x37, CheckoutButton.TEXT_PAY);
        	LinearLayout root = (LinearLayout) paymentView.findViewById(R.id.paypalLayout);
        	root.addView(ppButton);
        }
        Button otherPayment = (Button) paymentView.findViewById(R.id.otherPayment);
        
        
        paymentPopup = new AlertDialog.Builder(NewOrderActivity.this)
        .setView(paymentView)
        .setCancelable(false)
        .setNegativeButton("Retour � la commande", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		})
        .show();
        
        if(paypal){
	        //Clique sur PayPal
	        ppButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PayPalPayment newPayment = NewOrderActivity.this.getPayPalPayment(finalInstruction);
					
					Intent checkoutIntent = PayPal.getInstance().checkout(newPayment, NewOrderActivity.this);
					NewOrderActivity.this.startActivityForResult(checkoutIntent, NewOrderActivity.request);
					ppButton.updateButton();
				}
			});
        }
        
        //Clique sur autre paiement
        otherPayment.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				paymentPopup.hide();
				sendInstruction(finalInstruction);
			}
		});
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == NewOrderActivity.request){
			if(resultCode == Activity.RESULT_OK){
				this.paymentPopup.hide();
				this.sendInstruction(this.finalInstruction);
			}
		}
	}
	
	
	
	
	//Fragment management
	private void setFragmentVisibility(Fragment fragment, boolean visible, boolean animate){
		FragmentTransaction ft = this.fm.beginTransaction();
		if(animate) ft.setCustomAnimations(R.animator.rotate_show, R.animator.rotate_hide);
		if(visible){
			ft.show(fragment);
		} else {
			ft.hide(fragment);
		}
		ft.commit();
	}
	
	public void menuSelected(int position, InstructionMenu menu){
		this.menuFragment.setMenu(position, menu);
		this.setFragmentVisibility(this.menuFragment, true, true);
		this.setFragmentVisibility(this.productFragment, false, true);
	}
	
	public void productTypeSelected(int position){
		this.productFragment.setProductType(position);
		this.setFragmentVisibility(this.productFragment, true, true);
	}
	
	public void productSelected(Product p){
		this.menuFragment.setProduct(p);
		this.setFragmentVisibility(this.productFragment, false, true);
	}
	
	public void updateMenu(InstructionMenu menu){
		this.orderFragment.updateCurrentMenu(menu);
	}
	
	public void menuDeleted(){
		this.orderFragment.deleteCurrentMenu();
		this.setFragmentVisibility(this.menuFragment, false, true);
		this.setFragmentVisibility(this.productFragment, false, true);
	}
}
