package com.rman.youfoodcustomer.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.rman.youfoodcustomer.util.HttpHelper;
import com.rman.youfoodcustomer.util.Messages;

public class IndexActivity extends Activity {
    /** Called when the activity is first created. */
	
	private Button newOrderButton;
	private Long table;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        this.newOrderButton = (Button) findViewById(R.id.newOrderButton);
        
        this.newOrderButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(IndexActivity.this, NewOrderActivity.class);
				startActivity(intent);
			}
		});
        
        this.loadPreferences();
    }

    
    private void loadPreferences(){
    	SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
  		HttpHelper.setURI(sp.getString("address", "localhost"));
        this.table = Long.valueOf(sp.getString("table", "0"));
        if(this.table <= 0){
        	Toast.makeText(this, "Aucune table n'est configur�e.\nVeuillez configurer votre application.", Toast.LENGTH_LONG).show();
        	Intent intent = new Intent(IndexActivity.this, ConfigActivity.class);
        	startActivityForResult(intent, 2);
        }
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.layout.index_menu, menu);
    	
    	return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
		case R.id.configoption:
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle("Autorisation");
			alert.setMessage("Veuillez entrer le mot de passe");
			alert.setIcon(R.drawable.ic_settings2);

			final EditText input = new EditText(this);
			input.setTransformationMethod(PasswordTransformationMethod.getInstance());
			input.addTextChangedListener(new TextWatcher() {
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					
				}
			});
			alert.setView(input);

			alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			  String value = input.getText().toString();
			  SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(IndexActivity.this);
			  if(sp.getString("password", "").equals(value)){
				  Intent intent = new Intent(IndexActivity.this, ConfigActivity.class);
				  startActivityForResult(intent, 2);
			  } else {
				  Toast.makeText(IndexActivity.this, "Mot de passe incorrect", Toast.LENGTH_LONG).show();
			  }
			  }
			});

			alert.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
			  public void onClick(DialogInterface dialog, int id) {
			    dialog.cancel();
			  }
			});

			alert.show();
			return true;
					
		case R.id.helpoption:
			Messages.createAlert(this, "Aide", Messages.HELP, "Bienvenue chez YouFood !\n\n" +
					"Appuyez sur nouvelle commande pour commencer � commander votre repas.");
			return true;
		}
    	return false;
    }
    
    
    @Override
  	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
  		super.onActivityResult(requestCode, resultCode, data);
  		this.loadPreferences();
  	}
}