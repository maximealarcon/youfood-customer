package com.rman.youfoodcustomer.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.rman.youfoodcustomer.activity.R;

public class Messages {

	public static int ERROR = R.drawable.ic_error;
	public static int INFO = R.drawable.ic_info;
	public static int HELP = R.drawable.ic_idea;
	
	public static void createAlert(Context context, String message){
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
               .setCancelable(false)
               .setNegativeButton("Fermer", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                   }
               });
        AlertDialog alert = builder.create();
        alert.show();
	}
	
	public static void createAlert(Context context, String title, int alertType, String message){
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
        	   .setIcon(alertType)
        	   .setMessage(message)
               .setCancelable(false)
               .setNegativeButton("Fermer", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                   }
               });
        AlertDialog alert = builder.create();
        alert.show();
	}
}
