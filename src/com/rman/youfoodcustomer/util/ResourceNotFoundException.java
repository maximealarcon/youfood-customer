package com.rman.youfoodcustomer.util;


@SuppressWarnings("serial")
public class ResourceNotFoundException extends Exception {
	
	private String message;
	
	public ResourceNotFoundException(String message, String address) {
		this.message = message;
	}
	
	public ResourceNotFoundException(int errorCode, String address) {
		if (errorCode == 404){
			this.message = "Impossible de trouver la ressource sp�cifi�e";
		} else {
			this.message = "Une erreur est survenue pendant la r�cup�ration des donn�es";
		}
	}
	
	@Override
	public String getMessage() {
		return this.message;
	}
}
